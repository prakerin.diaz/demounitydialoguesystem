using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using PixelCrushers.DialogueSystem;
using System;
using Test.ServiceLocator;

public class PlayerMovement : MonoBehaviour
{
    GameManager gameManager => ServiceLocator.Current.Get<GameManager>();
    [SerializeField] float Speed;
    Vector2 move;
    PlayerState State;
    Vector3 tmp;
    Vector3 bark;

    private void Start()
    {
        bark = new Vector3(-6.81f, 1.61f, 4.19f);
        State = PlayerState.Walk;
    }
    void Update()
    {
        if (State == PlayerState.Walk && gameManager.GetState()==MainState.Play)
            MoveCharacter();
    }
    public void OnMove(InputAction.CallbackContext context)
    {
        move = context.ReadValue<Vector2>();
    }
    void MoveCharacter()
    {
        if (move.sqrMagnitude > 0.1f)
        {
            Vector3 movement = new Vector3(move.x, 0f, move.y);
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(movement), 0.15f);
            transform.Translate(movement * Speed * Time.deltaTime, Space.World);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "npc")
        {
            DialogueManager.StartConversation("Conversation Servant");
        }
        if(other.tag== "NpcCutscene")
        {
            if(gameManager.GetState() != MainState.Cutscene)
            gameManager.SetMainState(MainState.Cutscene);
        }
    }
    public void OnStartConversation()
    {
        State = PlayerState.Freze;
        Debug.Log("Start Conversation");
    }
    public void OnCloseConversation()
    {
        State = PlayerState.Walk;
        Debug.Log("Close");
    }
    public void OnStartBark()
    {
        Debug.Log("Start Bark");
    }
    public void OnCloseBark()
    {
        State = PlayerState.Walk;
    }
}
enum PlayerState
{
    Walk,
    Freze
}
