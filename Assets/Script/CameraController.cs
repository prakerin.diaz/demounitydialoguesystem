using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using Test.ServiceLocator;
using System.Threading.Tasks;

public class CameraController : MonoBehaviour
{
    [SerializeField] CinemachineVirtualCamera cam1;
    [SerializeField] CinemachineVirtualCamera cam2;
    GameManager gameManager => ServiceLocator.Current.Get<GameManager>();
    // Start is called before the first frame update
    void Start()
    {
        cam1.Priority = 1;
        cam2.Priority = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (gameManager.GetState() == MainState.Cutscene)
        {
            ChangeCamera();
        }

    }
    async void ChangeCamera()
    {
        cam1.Priority = 0;
        cam2.Priority = 1;

        await Task.Delay(10000);
        gameManager.SetMainState(MainState.Play);
        cam1.Priority = 1;
        cam2.Priority = 0;
    }
}
