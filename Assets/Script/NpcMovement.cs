using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Test.ServiceLocator;
using System.Threading.Tasks;

public class NpcMovement : MonoBehaviour
{
    GameManager gameManager => ServiceLocator.Current.Get<GameManager>();
    [SerializeField] Object[] Object;
    [SerializeField] Environvemt[] Envi;
    [SerializeField] Npc Npc;
    StateNpc stateNPC;
    private void Start()
    {
        stateNPC = StateNpc.Wait;
        MoveInteractableNpc();
    }
    async void MoveInteractableNpc()
    {
        Npc.prefab.transform.DOMoveZ(Npc.nextPos.z, 10f).OnComplete(() =>
        {
            Npc.prefab.transform.DORotate(new Vector3(0, 0, 0), 0.5f, RotateMode.Fast).SetEase(Ease.Linear).OnComplete(() =>
            {
                Npc.prefab.transform.DOMoveZ(Npc.curPos.z, 10f).OnComplete(() =>
                {
                    Npc.prefab.transform.DORotate(new Vector3(0, 180, 0), 0.5f, RotateMode.Fast).SetEase(Ease.Linear).OnComplete(() =>
                    {
                        MoveInteractableNpc();
                    });
                });
            });
        });
    }
    private void Update()
    {
        SwitchNpcState();
    }
    void SwitchNpcState()
    {
        if (gameManager.GetState() == MainState.Cutscene)
        {
            stateNPC = StateNpc.Cutscene;
        }
        if(gameManager.GetState() == MainState.Play)
        {
            stateNPC = StateNpc.Wait;
        }
    }
    private void FixedUpdate()
    {
        if (stateNPC != StateNpc.Cutscene) return;
        CutSceneMove();       
    }
    async void CutSceneMove()
    {
        ResetPosNpc();
        await Task.Delay(3000);
        foreach (var dino in Object)
        {
            dino.prefab.transform.DOMove(new Vector3(dino.nextPos.x, dino.nextPos.y, dino.nextPos.z), dino.time).SetEase(Ease.Linear);
        }
        foreach (var envi in Envi)
        {
            envi.prefab.transform.DORotate(new Vector3(0, 360, 0), envi.time * 0.5f, RotateMode.FastBeyond360).SetLoops(-1, LoopType.Restart).SetEase(Ease.Linear);
        }
    }
    void ResetPosNpc()
    {
        for(int i = 0; i < Object.Length; i++)
        {
            Object[i].ResetPos();
        }
    }
    void ChangeStateNpc(StateNpc state)
    {
        stateNPC = state;
    }
}
public enum StateNpc
{
    Wait,
    Play,
    Cutscene
}
[System.Serializable]
public class Object
{
    public GameObject prefab;
    public Vector3 curPost;
    public Vector3 nextPos;
    public float time;

    public void ResetPos()
    {
        prefab.transform.position = new Vector3(curPost.x, curPost.y, curPost.z);
    }
}
[System.Serializable]
public class Environvemt
{
    public GameObject prefab;
    public float time;
}

[System.Serializable]
public class Npc
{
    public GameObject prefab;
    public Vector3 curPos;
    public Vector3 nextPos;
}
