using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Test.ServiceLocator;

public class GameManager : IGameManager
{
    MainState mainState;
    public MainState GetState()
    {
        return mainState;
    }

    public void SetMainState(MainState main)
    {
        mainState = main;
    }
}
