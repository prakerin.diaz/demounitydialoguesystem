using UnityEngine;

namespace Test.ServiceLocator
{
    public static class Bootstraper
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        public static void Initiailze()
        {

            ServiceLocator.Initiailze();

            ServiceLocator.Current.Register(new GameManager());

        }
    }

}
