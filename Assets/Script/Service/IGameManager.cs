namespace Test.ServiceLocator
{
    public interface IGameManager : IGameService
    {
        void SetMainState(MainState main);
        MainState GetState();
    }
}